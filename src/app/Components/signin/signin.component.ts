import { AuthService } from './../../Providers/Auth/auth.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  user: any = {};
  disableBtn: any = false;
  lang: any;
  errors: string;
  constructor(public authService: AuthService, public translate: TranslateService, private router: Router) { }

  ngOnInit() {
    this.translate.get('components').subscribe((response: any) => {
      this.lang = response;
      console.log(this.lang);
    });
  }
  async signin() {
    this.disableBtn = true;
    console.log(this.user);
    this.user.type_register = 1;
    this.authService.signin(this.user).subscribe((response: any) => {
      this.disableBtn = false;
      if (response.data) {
        localStorage.setItem('token', response.data.access_token);
        localStorage.setItem('user', JSON.stringify(response.data.user));
        console.log(response);
        this.router.navigate(['/dashboard', { outlets: { sections: ['home'] } }]);
      } else {
        this.errors = '';
        let me = this;
        Object.keys(response).map(function (objectKey, index) {
          const value = response[objectKey];
          me.errors = me.errors + '<li>' + value[0] + '</li>';
          console.log('<li>' + value[0] + '</li>');
        });
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          html: '<ul class="list-unstyled">' + this.errors + '</ul>'
        });

      }
    }, (err: any) => {
      console.log(err);
      switch (err.status) {
        case 500:
          //las credenciales no coinciden
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: this.lang.login.sweet_alert.error.authentication_failed.text
          });
          break;
      }
      this.disableBtn = false;
    });
  }
}
