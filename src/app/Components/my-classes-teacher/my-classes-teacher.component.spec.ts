import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyClassesTeacherComponent } from './my-classes-teacher.component';

describe('MyClassesTeacherComponent', () => {
  let component: MyClassesTeacherComponent;
  let fixture: ComponentFixture<MyClassesTeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyClassesTeacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyClassesTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
