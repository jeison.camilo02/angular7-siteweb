import { Component, OnInit, Input  } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(public translate: TranslateService, public router: Router) { }
  btnDisabledEs: any = false;
  btnDisabledEn: any = false;
  @Input() showBtgLogin: any;
  @Input() typeNavbar: any;
  user: any;

  ngOnInit() {
    this.translate.get('lang').subscribe( value =>{
      if (value === 'es') {
        this.btnDisabledEs = true;
      } else {
        this.btnDisabledEn = true;
      }
    });
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
  }

  changeLang(lang: string) {

    this.translate.use(lang);
    this.translate.get('lang').subscribe( value =>{
      if (value === 'es') {
        this.btnDisabledEs = true;
        this.btnDisabledEn = false;
      } else {
        this.btnDisabledEn = true;
        this.btnDisabledEs = false;
      }
    });
  }
  sign_out() {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.user = null;
    this.router.navigate(['/']);
  }
  goToHome() {
    this.router.navigate(['/dashboard', { outlets: { sections: ['home'] }}]);
  }

}
