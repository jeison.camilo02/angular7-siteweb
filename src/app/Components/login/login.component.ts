import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Providers/Auth/auth.service';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  lang: any;
  email: any;
  password: any;
  disableBtn: any = false;

  constructor(public auhtService: AuthService, public translate: TranslateService, public router: Router) { }

  ngOnInit() {
    this.translate.get('components').subscribe((response: any) => {
      this.lang = response;
      console.log(this.lang);
    });
  }

  login() {
    this.disableBtn = true;
    const body = { email: this.email, password: this.password };
    this.auhtService.login(body).subscribe((response: any) => {
      localStorage.setItem('token', response.data.access_token);
      localStorage.setItem('user', JSON.stringify(response.data.user));
      console.log(response);
      this.router.navigate(['/dashboard', { outlets: { sections: ['home'] }}]);
      this.disableBtn = false;
    }, (err: any) => {
      switch(err.status){
        case 401:
        //las credenciales no coinciden
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: this.lang.login.sweet_alert.error.authentication_failed.text
        });
        break;
      }
      this.disableBtn = false;
    });
  }

}
