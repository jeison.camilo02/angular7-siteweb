import { Component, OnInit } from '@angular/core';
import { UserServicesService } from 'src/app/Providers/User/user-services.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: any;
  traComponents: any;
  rols = [
    {id : 1},
    {id : 2},
    {id : 3}
  ];
  constructor(public userServices: UserServicesService, private translate: TranslateService) { }

  ngOnInit() {
    this.translate.get('components').subscribe((translate: any) => {
      this.traComponents = translate;
      console.log(this.traComponents);
    });
    this.userServices.getAllUsers().subscribe((response: any) => {
      this.users = response.data;
    });

  }

}
