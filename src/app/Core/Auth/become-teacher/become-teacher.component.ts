import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-become-teacher',
  templateUrl: './become-teacher.component.html',
  styleUrls: ['./become-teacher.component.css']
})
export class BecomeTeacherComponent implements OnInit {
  statusRegister = 2;
  constructor() { }

  ngOnInit() {
  }

  step(type) {
    if (type === 'next') {
      this.statusRegister++;
    }
    if (type === 'previous') {
      this.statusRegister--;
    }
  }

}
