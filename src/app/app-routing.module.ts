import { ProfileComponent } from './Components/profile/profile.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './Core/Home/home/home.component';
import { MainAuthComponent } from './Core/Auth/main-auth/main-auth.component';
import { MainComponent } from './Core/Dashboard/main/main.component';
import { HomeDashboardComponent } from './Components/home-dashboard/home-dashboard.component';
import { ScheduleDashboardComponent } from './Components/schedule-dashboard/schedule-dashboard.component';
import { UsersComponent } from './Components/users/users.component';
import {ClassesComponent} from './Components/classes/classes.component';
import { MyClassesTeacherComponent } from './Components/my-classes-teacher/my-classes-teacher.component';
import { MyClassesComponent } from './Components/my-classes/my-classes.component';
import { ActivitiesListComponent } from './Components/activities-list/activities-list.component';
import { BecomeTeacherComponent } from './Core/Auth/become-teacher/become-teacher.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: MainAuthComponent }, //Auth
  { path: 'become_teacher', component: BecomeTeacherComponent }, //
  { path: 'dashboard', component: MainComponent,
    children: [
      {
        path: 'home',
        component: HomeDashboardComponent,
        outlet: 'sections'
      },
      {
        path: 'schedule',
        component: ScheduleDashboardComponent,
        outlet: 'sections'
      },
      {
        path: 'profile',
        component: ProfileComponent,
        outlet: 'sections'
      },
      {
        path: 'users',
        component: UsersComponent,
        outlet: 'sections'
      },
      {
        path: 'classes',
        component: ClassesComponent,
        outlet: 'sections'
      },
      {
        path: 'my-classes',
        component: MyClassesComponent,
        outlet: 'sections'
      },
      {
        path: 'my-classes-teacher',
        component: MyClassesTeacherComponent,
        outlet: 'sections'
      },
      {
        path: 'activities-list',
        component: ActivitiesListComponent,
        outlet: 'sections'
      }
    ]
}  //Dashboard
];
@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {}
