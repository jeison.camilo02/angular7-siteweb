import { environment as env} from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public http: HttpClient) {}
  getUsers() {
    return this.http.get(env.apiUrl + 'user');
  }
  signin(user: any) {
    return this.http.post(env.apiUrl + 'auth/register' , user );
  }
  login(user: any) {
    return this.http.post(env.apiUrl + 'auth/login' , user);
  }
}
