import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HomeComponent } from './Core/Home/home/home.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { ProgramsComponent } from './Components/programs/programs.component';
import { TeachersComponent } from './Components/teachers/teachers.component';
import { MainAuthComponent } from './Core/Auth/main-auth/main-auth.component';
import { LoginComponent } from './Components/login/login.component';
import { SigninComponent } from './Components/signin/signin.component';
import { HttpConfigInterceptor } from './Providers/httpconfig.interceptor';
import { FormsModule } from '@angular/forms';
import { MainComponent } from './Core/Dashboard/main/main.component';
import { HomeDashboardComponent } from './Components/home-dashboard/home-dashboard.component';
import { ScheduleDashboardComponent } from './Components/schedule-dashboard/schedule-dashboard.component';
import { ProfileComponent } from './Components/profile/profile.component';
import { UsersComponent } from './Components/users/users.component';
import { ClassesComponent } from './Components/classes/classes.component';
import { MyClassesComponent } from './Components/my-classes/my-classes.component';
import { MyClassesTeacherComponent } from './Components/my-classes-teacher/my-classes-teacher.component';
import { ActivitiesListComponent } from './Components/activities-list/activities-list.component';
import { BecomeTeacherComponent } from './Core/Auth/become-teacher/become-teacher.component';


// i18n
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProgramsComponent,
    TeachersComponent,
    MainAuthComponent,
    LoginComponent,
    SigninComponent,
    MainComponent,
    HomeDashboardComponent,
    ScheduleDashboardComponent,
    ProfileComponent,
    UsersComponent,
    ClassesComponent,
    MyClassesComponent,
    MyClassesTeacherComponent,
    ActivitiesListComponent,
    BecomeTeacherComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
